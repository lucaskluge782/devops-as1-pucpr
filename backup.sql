-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               8.0.31 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for atp
CREATE DATABASE IF NOT EXISTS `atp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `atp`;

-- Dumping structure for table atp.emprestimos
CREATE TABLE IF NOT EXISTS `emprestimos` (
  `id_user` int NOT NULL,
  `id_livro` int NOT NULL,
  `data_emprestimo` datetime NOT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atp.emprestimos: ~0 rows (approximately)
DELETE FROM `emprestimos`;
INSERT INTO `emprestimos` (`id_user`, `id_livro`, `data_emprestimo`, `status`, `id`) VALUES
	(1, 1, '2323-03-26 09:03:36', 'fechado', 1),
	(1, 1, '2323-03-26 09:03:24', 'aberto', 2);

-- Dumping structure for table atp.itens
CREATE TABLE IF NOT EXISTS `itens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8mb4_unicode_ci,
  `data_cad` datetime NOT NULL,
  `status` enum('disponivel','indisponivel') COLLATE utf8mb4_unicode_ci NOT NULL,
  `avaliacao` int DEFAULT '0',
  `autor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atp.itens: ~0 rows (approximately)
DELETE FROM `itens`;
INSERT INTO `itens` (`id`, `titulo`, `descricao`, `data_cad`, `status`, `avaliacao`, `autor`) VALUES
	(1, 'Primeiro livro', 'item de teste', '2323-03-26 09:03:10', 'indisponivel', 0, 'eu mesmo'),
	(2, 'Segundo livro', 'Segundo livro de teste', '2323-03-26 09:03:14', 'disponivel', 0, 'Desconhecido');

-- Dumping structure for table atp.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nivel` enum('usuario','administrador') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `senha` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table atp.usuarios: ~0 rows (approximately)
DELETE FROM `usuarios`;
INSERT INTO `usuarios` (`id`, `nome`, `email`, `nivel`, `fone`, `senha`) VALUES
	(1, 'Lucas', 'lucas@email.com', 'administrador', '419111111', '7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d'),
	(2, 'Visitante', 'visitante@email.com', 'usuario', NULL, 'cc8b92a5d5467e052abeb80590741c336242b9f2d4b322af2958cbef50de77b20df67e01047d36bcfe3592037409122469ef51d8ab0c85707e24e48ed2e6a1cd');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
