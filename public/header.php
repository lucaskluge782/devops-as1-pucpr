<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biblioteka</title>
    <link rel="stylesheet" href="./public/style.css">
</head>
<body>
<div class="navbar">
    <a href="#" class="navtitle">Biblioteka</a>
    <a href="?p=cad_item">Cadastrar livro</a>
    <a href="?p=itens">Itens</a>
    <?php if(isLogged()): ?>
        <a href="?p=painel">Painel</a>
        | <a href="?p=perfil">Perfil</a>
          <a href="?p=login&logout=true">Sair</a>
    <?php else: ?>
        <a href="?p=cadastro_user">Cadastrar-se</a>
        <a href="?p=login">Login</a>
    <?php endif; ?>
</div>