<?php

    if(isset($_POST['user'],$_POST['email'],$_POST['pass'],$_POST['pass2'])){
        
        // Senha e confirmação senha são iguais
        if($_POST['pass'] == $_POST['pass2']){
            require_once 'classes/Usuario.php';
            (new Usuario)->cadastro();
        }
    }
?>

<div class="painel painel-small">
    <h2 class="title">Cadastro</h2>
    <br><?=hasMsg()?>
    <form action="#" method="POST">
        <label for="user">Nome </label>
        <input type="text" name="user" id="user" required><br><br>
        <label for="email">Email </label>
        <input type="email" name="email" id="email" required><br><br>
        <label for="pass">Senha </label>
        <input type="password" name="pass" id="pass" required><br><br>
        <label for="pass2">Confirmar Senha </label>
        <input type="password" name="pass2" id="pass2" required><br><br>
        <div id="btn">
        <a href="?p=login" style="padding-right:8px">Login</a>
        <button type="submit" class="btn">Cadastrar</button>
        </div>
    </form>
</div>