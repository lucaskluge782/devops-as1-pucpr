<?php
if (! isLogged()){
    redirect('login');
}

require_once 'classes/Database.php';
$db = new Database('127.0.0.1', 'atp', 'root', 'root');
$itens = $db->selectAll('emprestimos', '*');
?>

<div class="painel">
    <?php if($itens) : ?>
        <!-- VARIOS ITENS -->
        <h2 class="title">Últimos Empréstimos <a href="?p=cad_item">Adicionar</a></h2>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Autor (a)</th>
                    <th>Data empréstimo</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($itens as $item): ?>
                    <tr>
                        <td><a href="?p=itens&item=<?=$item['id']?>"><?=$item['titulo']?></a></td>
                        <td><?=$item['descricao']?></td>
                        <td><?=$item['autor']?></td>
                        <td><?=strftime('%d/%m/%Y', strtotime($item['data_cad']))?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    <?php endif; ?>
</div>