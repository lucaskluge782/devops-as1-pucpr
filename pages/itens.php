<?php
if (! isLogged()){
    redirect('login');
}


require_once 'classes/Database.php';
$db = new Database('127.0.0.1', 'atp', 'root', 'root');

if(isset($_GET['item'])){
    $id = intval($_GET['item']);
    $singleItem = $db->selectAll('itens', '*', "id = '$id'");
}
else{
    $itens = $db->selectAll('itens', '*');
}

// Emprestar item
if(isset($_GET['item']) && isset($_GET['emprestar'])){

    require_once 'classes/Usuario.php';
    $user = new Usuario();

    $id = intval($_GET['item']);
    $data = Date('yy-m-d h:m:s');
    $emprestar = $user->emprestar(isLogged()[0]['id'], $id, $data);

    if($emprestar){
        redirect('home');
    }
    else{
        redirect('itens&item='.$id);
    }
}
?>

<div class="painel">
    <?php if($itens) : ?>
        <!-- VARIOS ITENS -->
        <h2 class="title">Últimos Empréstimos <a href="?p=cad_item">Adicionar</a></h2>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Autor (a)</th>
                    <th>Data Registro</th>
                    <th>Avaliação</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($itens as $item): ?>
                    <tr>
                        <td><a href="?p=itens&item=<?=$item['id']?>"><?=$item['titulo']?></a></td>
                        <td><?=$item['descricao']?></td>
                        <td><?=$item['autor']?></td>
                        <td><?=strftime('%d/%m/%Y', strtotime($item['data_cad']))?></td>
                        <td><?=($item['avaliacao'] >0 && $item['avaliacao'] < 10)? '<img src="public/img/star_icon_2.png" class="icon"/>':''?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    <?php elseif($singleItem): ?>
        <!-- ITEM ESPECIFICO -->
        <h2 class="title">Informações do Livro</h2><br><br>
        <div class="left-perfil">
            <img src="public/img/book.png" alt="Livro" class="img-book"><br><br>
            <p><?=($singleItem[0]['status'] == 'disponivel') ? "<span style='color:green'>".ucfirst($singleItem[0]['status'])."</span>" : "<span style='color:red'>".ucfirst($singleItem[0]['status'])."</span>"?></p>
        </div>
        <div class="perfil-right">
            <h2 class="htitle"><?=$singleItem[0]['titulo']?></h2>
            <p><strong>Autor (a):</strong> <?=$singleItem[0]['autor']?></p>
            <br>
            <p><strong>Descrição: </strong><?=$singleItem[0]['descricao']?></p><br><br>

            <a href="?p=itens&item=<?=$singleItem[0]['id']?>&emprestar=true" class="btn">EMPRESTAR</a>
        </div>
    <?php endif; ?>
</div>