<?php

if (! isLogged()){
    redirect('login');
}

    require_once 'classes/Usuario.php';
    $user  = new Usuario();
    $editar = true;
    // Retorna informações de usuário específico
    if(isset($_GET['id'])){
        $dados = $user->perfil(intval($_GET['id']));
        $editar = (isLogged()[0]['nivel'] == 'usuario') ? false : true;
    }
    else{
        // Retorna informações do usuário logado
        $dados = $user->perfil();
    }

    // Atualiza informações do usuario
    if(isset($_POST['user'], $_POST['email'], $_POST['nivel']) && $editar === true){
        $update = $user->atualiza($_POST['userid']);
        
        if($update){
            setMsg('success', "Dados atualizados com sucesso!");
            redirect('home');
        }
    }

    
?>

<div class="painel">
    <?php if(isset($dados[0])) : ?>
        <h2 class="title">Perfil do Usuário</h2><br><br>
        <div class="left-perfil">
            <img src="public/img/user.png" alt="Perfil" class="img-perfil"><br>
            <h2 class="htitle"><?=$dados[0]['nome']?></h2>
            <p><?=$dados[0]['email']?></p>
            <p><?=ucfirst($dados[0]['nivel'])?></p>
        </div>
        <div class="perfil-right">
            <h3 class="htitle">Atividades deste usuário</h3><br>
            <div class="alert alert-error">Não há atividades a serem exibidas!</div><br><br>
            <h3 class="htitle">Editar Perfil</h3><br>
            <form action="#" method="POST">
                <label for="user">Nome </label>
                <input type="text" name="user" id="user" required value="<?=$dados[0]['nome']?>" <?=(!$editar)?'disabled':''?>><br><br>
                <label for="email">Email </label>
                <input type="email" name="email" id="email" required value="<?=$dados[0]['email']?>" <?=(!$editar)?'disabled':''?>><br><br>
                <label for="fone">Fone </label>
                <input type="text" name="fone" id="fone" required value="<?=$dados[0]['fone']?>" <?=(!$editar)?'disabled':''?>><br><br>               
                <label for="nivel">Nivel </label>
                <select name="nivel" id="nivel" value="<?=$dados[0]['nivel']?>" <?=(!$editar)?'disabled':''?>>
                    <option value="usuario" <?=($dados[0]['nivel'] == 'usuario') ? 'selected' : ''?>>Usuário</option>
                    <option value="administrador" <?=($dados[0]['nivel'] == 'administrador') ? 'selected' : ''?>>Administrador</option>
                </select><br><br>
                <input type="hidden" name="userid" value="<?=$dados[0]['id']?>" readonly>
                <div id="btn">
                    <button type="submit" class="btn" <?=(!$editar)?'disabled':''?>>Editar</button>
                </div>
            </form><br>
        </div>
    <?php endif; ?>
</div>