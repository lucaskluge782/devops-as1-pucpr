<?php

if (! isLogged()){
    redirect('login');
}

require_once 'classes/Database.php';
$db = new Database('127.0.0.1', 'atp', 'root', 'root');
$itens    = $db->selectAll('itens', '*',null, 'ORDER BY id DESC LIMIT 10');
$usuarios = $db->selectAll('usuarios', '*');

?>

<div class="painel">
    <?php if($itens) : ?>
        <h2 class="title">Últimos Livros Cadastrados</h2>
        <table>
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Data Registro</th>
                    <th>Avaliação</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($itens as $item): ?>
                    <tr>
                        <td><a href="?p=itens&item=<?=$item['id']?>"><?=$item['titulo']?></a></td>
                        <td><?=$item['descricao']?></td>
                        <td><?=strftime('%d/%m/%Y', strtotime($item['data_cad']))?></td>
                        <td><?=($item['avaliacao'] >0 && $item['avaliacao'] < 10)? '<img src="public/img/star_icon_2.png" class="icon"/>':''?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<!-- USUARIOS -->
<div class="painel">
    <?php if($itens) : ?>
        <h2 class="title">Usuários registrados</h2>
        <table>
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Fone</th>
                    <th>Nível</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($usuarios as $usuario): ?>
                    <tr>
                        <td><a href="?p=perfil&id=<?=$usuario['id']?>"><?=$usuario['nome']?></a></td>
                        <td><?=$usuario['email']?></td>
                        <td><?=$usuario['fone']?></td>
                        <td><?=($usuario['nivel'] == 'administrador') ? "<span style='color:red'>".ucfirst($usuario['nivel'])."</span>" : ucfirst($usuario['nivel'])?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    <?php endif; ?>
</div>