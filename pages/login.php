<?php
if(isset($_POST['email'], $_POST['pass'])){
    require_once 'classes/Usuario.php';
    $user = new Usuario();
    $user->login();
}

if(isset($_GET['logout'])){
    require_once 'classes/Usuario.php';
    $user = new Usuario();
    $user->logout();
}
?>
<div class="painel painel-small">
    <h2 class="title">Login</h2>
    <br><?=hasMsg()?>
    <form action="?p=login" method="POST">
        <label for="email">Email </label>
        <input type="email" name="email" id="email" required><br><br>
        <label for="pass">Senha </label>
        <input type="password" name="pass" id="pass" minlength="8" required><br><br> 
        <div id="btn">
        <a href="?p=cadastro_user" style="padding-right:8px">Cadastrar-se</a>
        <button type="submit" class="btn">Entrar</button>
        </div>
    </form>
</div>