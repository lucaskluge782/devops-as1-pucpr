<?php
if (! isLogged()){
    redirect('login');
}

if(isset($_POST['tit'])){
    // Recupera os campos do formulário
    $data  = Date('yy-m-d h:m:s');
    $tit   = htmlspecialchars($_POST['tit']);
    $desc  = htmlspecialchars($_POST['desc']);
    $autor = htmlspecialchars($_POST['autor']);

    require_once 'classes/Database.php';
    $db = new Database('127.0.0.1', 'atp', 'root', 'root');
    // $itens = $db->selectAll('itens', '*');
    $campos = 'titulo, descricao, data_cad, status, autor';
    $valores = "'$tit','$desc', '$data', 'disponivel', '$autor'";
    $cad = $db->insert('itens', $campos, $valores);
    if($cad){
        redirect('cad_item');
    }else{
        die('Dados não inseridos');
    }
}
?>

<div class="painel painel-small">
    <h2 class="title">Cadastrar Item</h2>
    <form action="#" method="POST">
        <label for="tit">Titulo </label>
        <input type="text" name="tit" id="tit" required><br><br>
        <label for="desc">Descrição </label>
        <input type="text" name="desc" id="desc" required><br><br>
        <label for="autor">Autor (a) </label>
        <input type="text" name="autor" id="autor" required><br><br>
        <div id="btn">
        <button type="submit" class="btn">Cadastrar</button>
        </div>
    </form>
</div>