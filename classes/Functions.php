<?php
function ver($param, $exit = true){
    echo "<pre style='color:red'>";
    print_r($param);
    echo "</pre>";
    
    if($exit){
        exit;
    }
}

/**
 * ---------------------------------------------
 * FUNÇÕES DE SESSÃO
 * ---------------------------------------------
 */

// Verifica se o usuário está logado
function isLogged(){
    return isset($_SESSION['user_logged']) ? $_SESSION['user_logged'] : false;
}

// Adiciona usuário logado em uma $_SESSION
function userLogged($dados)
{
    unset($dados[0]['senha']);
    $_SESSION['user_logged'] = $dados;
}

// Cria uma mensagem de alerta "error" / "success"
function setMsg(String $tipo, String $msg)
{
    $_SESSION['msg'] = ['tipo' => $tipo, 'text' => $msg];
}

// Verifica se existe uma mensagem de alerta
function hasMsg()
{
    if(isset($_SESSION['msg'])):
        $tipo = $_SESSION['msg']['tipo'];
        $text = $_SESSION['msg']['text'];

        echo "<div class='alert alert-$tipo'>$text</div>";
        unset($_SESSION['msg']);
    else:
        return false;
    endif;
}


/**
 * ---------------------------------------------
 * FUNÇÕES DE URL
 * ---------------------------------------------
 */
function redirect($pagina)
{
    header("Location: ?p=$pagina");
    exit();
}