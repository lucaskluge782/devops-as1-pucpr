<?php
/**
 * ATP_WEBDEV
 * 
 * Classe genérica para conexão e operações com
 * o banco de dados. Utiliza PDO (https://www.php.net/manual/en/book.pdo.php).
 * 
 * Classe:  Database
 * Autor:   Lucas Kluge de Deus
 * Data:    25/11/2020
 */

 class Database
 {
    private $dns;
    private $db;
    private $user;
    private $pass;
    protected $conn;

    public function __construct($dns, $db, $user, $pass)
    {
        $this->dns  = $dns;
        $this->db   = $db;
        $this->user = $user;
        $this->pass = $pass;
        
        $this->connection();
    }

    public function connection()
    {
        try {
            $this->conn = new PDO("mysql:dbname=$this->db;host=$this->dns;charset=utf8", $this->user, $this->pass);

        } catch (PDOException $ex) {
            throw $ex;
        }
    }

    public function isConnected()
    {
        return ($this->conn) ? true : false;
    }

    public function selectAll($table, $fields, $where = null, $options = false)
    {
        $where = isset($where) ? " WHERE $where" : null;
        $sql   = "SELECT $fields FROM $table $where $options";
        $consulta = $this->conn->query($sql);
        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($table, $campos, $valores)
    {
        $sql = "INSERT INTO $table ($campos) VALUES ($valores)";
        return ($this->conn->exec($sql)) ? true : false;
    }

    public function update($table, $campos, $where = false)
    {
        $where = isset($where) ? " WHERE $where" : null;
        $sql = "UPDATE $table SET $campos $where ";
        $prep = $this->conn->prepare($sql);

       return ($prep->execute()) ? true : false;
    }

    public function delete($table, $where)
    {
        $where = isset($where) ? " WHERE $where" : null;
        $sql = "DELETE FROM $table $where ";

        return ($this->conn->exec($sql)) ? true : false;
    }
 }
 