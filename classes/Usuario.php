<?php
/**
 * ATP_WEBDEV
 * 
 * Classe para operações de usuário, como login, cadastro, etc.
 * 
 * Classe:  Usuario
 * Autor:   Lucas Kluge de Deus
 * Data:    25/11/2020
 */
class Usuario
{
    private $db;

    public function __construct()
    {
        require_once 'Database.php';
        try {
            $this->db = new Database('127.0.0.1', 'atp', 'root', 'root');
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    public function login()
    {
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $senha = hash('sha512', $_POST['pass']);
        $verifica_db = $this->db->selectAll('usuarios', '*',"email = '$email' AND senha = '$senha' LIMIT 1");
     
        if($verifica_db){
            userLogged($verifica_db);
            redirect('itens');
        }
        else{
            setMsg('error', "Usuário / email não encontrados!");
            redirect('login');
        }
    }

    /**
     * ---------------------------------------------
     * CADASTRO - Realiza cadastro do usuário 
     * mediante validação (bloqueia cadastro repetido)
     * ---------------------------------------------
     */
    public function cadastro()
    {
        $nome  = htmlspecialchars($_POST['user']);
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $senha = hash('sha512', $_POST['pass']);

        $verifica_db = $this->db->selectAll('usuarios', '*',"nome = '$nome' OR email = '$email'");
        
        // Usuario / email ja registrado
        if($verifica_db){
            setMsg('error', "Usuário / email em uso. Escolha outras opções!");
            redirect('cadastro_user');
        }
        // Usuaŕio / email disponiveis, realiza o cadastro 
        else{
            $cadastro = $this->db->insert('usuarios', "nome, email, senha", "'$nome', '$email', '$senha'");
            
            if($cadastro){
                setMsg('success', "Cadastro realizado com sucesso!");
                redirect('login');
            }
        }
    }

    /**
     * ---------------------------------------------
     * PERFIL - Retorna informações do prefil logado
     * ou usuário especifico, parametro $userid
     * ---------------------------------------------
     */
    public function perfil($userid = false)
    {
        if(!$userid){
            return isLogged();
        }
        else{
            $verifica_db = $this->db->selectAll('usuarios', '*',"id = '$userid'");
            return ($verifica_db) ? $verifica_db : false;
        }
    }

    //-- não terminado, trabalhar nisso
    public function atualiza($id)
    {
        $nome  = htmlspecialchars($_POST['user']);
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $nivel = htmlspecialchars($_POST['nivel']);
        $fone  = htmlspecialchars($_POST['fone']);

        $update = $this->db->update('usuarios', "nome = '$nome',
                                                email = '$email',
                                                nivel = '$nivel',
                                                fone  = '$fone'", "id = '$id'");
        return ($update) ? true : false;
    }

    /**
     * ---------------------------------------------
     * LOGOUT
     * ---------------------------------------------
     */
    public function logout()
    {
        $_SESSION = [];
        $cookies = session_get_cookie_params();
        // Deleta os cookies
        setcookie(
            session_name(),
            '',
            time() - 42000,
            $cookies['path'],
            $cookies['domain'],
            $cookies['secure'],
            $cookies['httponly'],
        );
        unset($_SESSION);
        session_destroy();
        redirect('home');
    }


    public function emprestar($iduser, $idlivro, $data)
    {
        $cadastro = $this->db->insert('emprestimos', "id_user, id_livro, data_emprestimo, status", 
                "'$iduser', '$idlivro', '$data', 'aberto'");
         
        if($cadastro){
            $update = $this->db->update('itens', "status = 'indisponivel'", "id = '$idlivro'");
            return ($update) ? true : false;
        }
        else{
            return false;
        }
    }
}