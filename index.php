<?php
session_start();

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

// Incluí o arquivo de funções
include_once 'classes/Functions.php';


// Incluí o cabeçalho HTML
require_once 'public/header.php';

// Incluí a página inicial
$page = (isset($_GET['p'])) ? htmlspecialchars(trim($_GET['p'])) : 'home';
$page = htmlspecialchars($page);
$page = (file_exists('./pages/'.$page.'.php')) ? $page : '404';
require_once './pages/'.$page.'.php';
// FIM Incluí a página inicial

// Incluí o rodapé HTML
require_once 'public/footer.html';